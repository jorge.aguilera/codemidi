package com.puravida.codemidi.midi

import com.puravida.codemidi.PlayMidi
import groovy.transform.CompileStatic

import javax.sound.midi.MidiChannel
import javax.sound.midi.MidiSystem
import javax.sound.midi.Synthesizer

/**
 * Created by jorge on 14/02/17.
 */
@CompileStatic
@Singleton(strict = true)
class Player {

    class Item{
        int channel
        int note
        long start
        long end
        Item(int channel, int note){
            this.channel=channel
            this.note = note
            this.start=Calendar.instance.timeInMillis
        }
    }

    List<Item> partiture = []

    void start(int channel, String note) {
        start(channel, Notes.fromString(note))
    }

    void start(int channel, int note){
        println "add $channel $note"
        partiture.add( new Item(channel, note) )
    }

    void stop(){
        partiture.last().end = Calendar.instance.timeInMillis
    }

    void playPartiture(){
        Synthesizer synthesizer = MidiSystem.synthesizer
        synthesizer.open()
        MidiChannel[] channels = synthesizer.channels
        partiture.each{ item ->
            println "${item.note}"
            if( -1 < item.channel && item.channel < channels.length)
                channels[item.channel].noteOn(item.note, 200);
            pause( (item.end - item.start) )
        }
    }

    void pause(long duration) {
        Thread.sleep(duration)
    }

}
