package com.puravida.codemidi.midi

/**
 * Created by jorge on 14/02/17.
 */
class Notes {

    public static final int A3 = 45
    public static final int AS3 = A3+1
    public static final int B3 = A3+2
    public static final int C3 = A3+3
    public static final int CS3 = A3+4
    public static final int D3 = A3+5
    public static final int DS3 = A3+6
    public static final int E3 = A3+7
    public static final int F3 = A3+8
    public static final int FS3 = A3+9
    public static final int G3 = A3+10
    public static final int GS3 = A3+11

    public static final int A4 = A3+12
    public static final int AS4 = A3+13
    public static final int B4 = A3+14
    public static final int C4 = A3+15
    public static final int CS4 = A3+16
    public static final int D4 = A3+17
    public static final int DS4 = A3+18
    public static final int E4 = A3+19
    public static final int F4 = A3+20
    public static final int FS4 = A3+21
    public static final int G4 = A3+22
    public static final int GS4 = A3+23

    public static int fromString(String str){
        switch( str ){
            case 'A3': return A3

            default: return GS4
        }
    }
}