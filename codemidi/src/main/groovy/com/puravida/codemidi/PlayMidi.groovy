package com.puravida.codemidi
/**
 * Created by jorge on 14/02/17.
 */
import asteroid.Local
import com.puravida.codemidi.ast.PlayMidiTransform

@Local(value=PlayMidiTransform, applyTo = Local.TO.METHOD)
@interface PlayMidi {

    NOTE value() default NOTE.A3

    static enum NOTE {
        A3,
        AS3,
        B3,
        C3,
        CS3,
        D3,
        DS3,
        E3,
        F3,
        FS3,
        G3,
        GS3,

        A4,
        AS4,
        B4,
        C4,
        CS4,
        D4,
        DS4,
        E4,
        F4,
        FS4,
        G4,
        GS4,
    }
}