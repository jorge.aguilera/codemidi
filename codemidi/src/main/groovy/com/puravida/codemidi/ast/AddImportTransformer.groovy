package com.puravida.codemidi.ast

import asteroid.A
import asteroid.transformer.AbstractClassNodeTransformer
import com.puravida.codemidi.CodeMidi
import com.puravida.codemidi.midi.Player
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.control.SourceUnit


/**
 * Created by jorge on 4/02/17.
 */
class AddImportTransformer extends AbstractClassNodeTransformer {

    public AddImportTransformer(final SourceUnit sourceUnit) {
        super(sourceUnit, byAnnotationName(CodeMidi.simpleName))
    }

    /**
     * {@inheritDocs}
     */
    @Override
    void transformClass(final ClassNode target) {
        A.UTIL.CLASS.addImport(target, Player)
    }
}