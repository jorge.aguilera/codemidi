package com.puravida.codemidi.ast

/**
 * Created by jorge on 14/02/17.
 */
import asteroid.A
import asteroid.Phase
import asteroid.Phase.LOCAL
import asteroid.AbstractLocalTransformation
import asteroid.Utils
import asteroid.nodes.AnnotationNodeBuilder
import asteroid.transformer.AbstractClassNodeTransformer
import com.puravida.codemidi.CodeMidi
import com.puravida.codemidi.midi.Player
import groovy.transform.CompileStatic

import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.builder.AstBuilder
import org.codehaus.groovy.ast.expr.*
import org.codehaus.groovy.ast.stmt.*
import org.codehaus.groovy.control.*
import org.codehaus.groovy.ast.tools.GeneralUtils
import org.junit.AfterClass

@CompileStatic
@Phase(LOCAL.SEMANTIC_ANALYSIS)
class CodeMidiTransform extends AbstractLocalTransformation<CodeMidi, ClassNode>{

    @Override
    void doVisit(final AnnotationNode annotation, final ClassNode node) {
        Integer channel = Utils.ANNOTATION.get(annotation, 'channel', Integer)
        if( channel == null){
            addError("Channel attribute is required", annotation)
            return
        }

        MethodNode afterClassMethod = A.NODES
                .method('playPartiture')
                .modifiers(A.ACC.ACC_STATIC|A.ACC.ACC_PUBLIC)
                .code(playPartiture())
                .build()
        afterClassMethod.returnType = ClassHelper.VOID_TYPE

        afterClassMethod.addAnnotation(AnnotationNodeBuilder.annotation(AfterClass).build())

        A.UTIL.CLASS.addMethod(node, afterClassMethod)

        A.UTIL.CLASS.addProperty(node, A.NODES
                .property('codeMidiChannel')
                .modifiers(A.ACC.ACC_PRIVATE)
                .type(Integer)
                .initialValueExpression(initChannelExpr(channel))
                .build()
        )
    }


    Expression initChannelExpr(Integer channel){
        A.EXPR.constX(channel)
    }

    Statement playPartiture(){
        A.STMT.blockS(
                A.STMT.stmt(A.EXPR.callX(A.EXPR.staticCallX(Player,'getInstance'),'playPartiture')),
        )
    }
}
