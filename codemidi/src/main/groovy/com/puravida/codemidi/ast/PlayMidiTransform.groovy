package com.puravida.codemidi.ast

/**
 * Created by jorge on 14/02/17.
 */
import asteroid.A
import asteroid.Phase
import asteroid.Phase.LOCAL
import asteroid.AbstractLocalTransformation
import asteroid.Utils
import com.puravida.codemidi.PlayMidi
import com.puravida.codemidi.midi.Player
import groovy.transform.CompileStatic

import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.expr.ConstantExpression
import org.codehaus.groovy.ast.stmt.*

@CompileStatic
@Phase(LOCAL.SEMANTIC_ANALYSIS)
class PlayMidiTransform extends AbstractLocalTransformation<PlayMidi, MethodNode>{

    @Override
    void doVisit(final AnnotationNode annotation, final MethodNode methodNode) {
        String value = Utils.ANNOTATION.get(annotation, 'value', String)
        if( !value ){
            addError("Note attribute is required", annotation)
            return
        }
        ConstantExpression ce = A.EXPR.constX(value)
        def oldCode   = methodNode.code
        def startCode = startPlay( ce )
        def endCode   = stopPlay()
        methodNode.code = A.STMT.blockS(startCode, oldCode, endCode)
    }

    Statement startPlay( ConstantExpression ce ) {
        return A.STMT.stmt(
                A.EXPR.callX(A.EXPR.staticCallX(Player,'getInstance'),'start',
                        A.EXPR.varX('codeMidiChannel'),
                        ce
                )
        )
    }

    Statement stopPlay(  ) {
        return A.STMT.stmt(
                A.EXPR.callX(A.EXPR.staticCallX(Player,'getInstance'),'stop')
        )
    }

}
