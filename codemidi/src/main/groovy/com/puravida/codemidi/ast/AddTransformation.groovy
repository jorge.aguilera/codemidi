package com.puravida.codemidi.ast

import asteroid.AbstractGlobalTransformation
import asteroid.Phase
import asteroid.transformer.Transformer
import groovy.transform.CompileStatic

import static asteroid.Phase.GLOBAL

/**
 * Created by jorge on 4/02/17.
 */
@CompileStatic
@Phase(GLOBAL.CONVERSION)
class AddTransformation extends AbstractGlobalTransformation {

    @Override
    List<Class<Transformer>> getTransformers() {
        return [AddImportTransformer]
    }
}
