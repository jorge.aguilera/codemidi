package com.puravida.codemidi
/**
 * Created by jorge on 14/02/17.
 */
import asteroid.Local
import com.puravida.codemidi.ast.CodeMidiTransform

@Local(value=CodeMidiTransform)
@interface CodeMidi {
    int channel()
}