# CodeMidi

Anotación para poner música a tu test

## CodeMidi

````java
@CodeMidi(channel = 4)
@RunWith(JUnit4.class)
class SimpleTest extends GroovyTestCase {

    @Test
    @PlayMidi(PlayMidi.NOTE.A3)
    void test1(){
        // hagamos que esto tarde un poco
        (0..10).each {
            sleep(it)
        }
    }

}
````
