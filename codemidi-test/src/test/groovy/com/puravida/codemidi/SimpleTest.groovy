package com.puravida.codemidi

import com.puravida.codemidi.midi.Player
import org.junit.AfterClass
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * Created by jorge on 14/02/17.
 */
@CodeMidi(channel = 4)
@RunWith(JUnit4.class)
class SimpleTest extends GroovyTestCase {

    @Test
    @PlayMidi(PlayMidi.NOTE.A3)
    void test1(){
        // hagamos que esto tarde un poco
        (0..10).each {
            sleep(it)
        }
    }

    @Test
    @PlayMidi(PlayMidi.NOTE.AS3)
    void test2(){
        // hagamos que esto tarde un poco
        (0..15).each {
            sleep(it)
        }
    }

    @Test
    @PlayMidi(PlayMidi.NOTE.A4)
    void test3(){
        // hagamos que esto tarde un poco
        (0..23).each {
            sleep(it)
        }
    }

    @Test
    @PlayMidi(PlayMidi.NOTE.B4)
    void test4(){
        // hagamos que esto tarde un poco
        (0..13).each {
            sleep(it*17)
        }
    }

}
